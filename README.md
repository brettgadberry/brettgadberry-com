# brettgadberry-com

This is the source for the https://brettgadberry.com/ site.

## Using Jekyll locally

To work locally with this project, follow the steps below:

1. [Install][] Jekyll
1. Download dependencies: `bundle install`
1. Build and preview: `bundle exec jekyll serve`

Read more at Jekyll's [documentation][].

[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
