---
layout: default
---

Experienced Customer Success Account Manager with a demonstrated history of working in the computer software industry. Skilled in DevSecOps, Cloud Management, and Technical Account Management. Strong engineering professional with a Bachelor’s Degree focused in Chemical Engineering from Arizona State University.
